//
//  AppDesign.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import UIKit

enum AppDesign {
    
    enum Color: String {
        
        case background = "background"
        
        
        var ui: UIColor {
            
            return UIColor(named: self.rawValue) ?? .black
        }
        
        var cg: CGColor {
            
            return ui.cgColor
        }
        
    }
    
}


extension AppDesign {
    
    enum Icon: String {
        
        case addContact = "addContact"
        
        var value: UIImage {
            
            return UIImage(named: self.rawValue) ?? UIImage()
        }
        
    }
    
}

