//
//  ContactCellModel.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import Foundation

enum CellModelType {
    case name
    case surname
    case date
    case company
    case email
}

class ContactCellModel {
    
    let title: String
    var value: String?
    let type: CellModelType
    
    init(title: String,
         value: String?,
         type: CellModelType) {
        self.title = title
        self.value = value
        self.type = type
    }
}
