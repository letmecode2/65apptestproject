//
//  ContactManager.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

protocol ContactsManagerInput: AnyObject {
    
    func didUpdate(contacts: [Contact])
}

final class ContactsManager: NSObject {
    
    // MARK: - Public properties
    static var shared = ContactsManager()
    
    // MARK: - Private properties
    private override init() {}
    
    weak var input: ContactsManagerInput?
    
    private var contacts = [Contact]()
    
    func getContacts() -> [Contact] {
        return contacts
    }
    
    func addContact(contact: Contact) {
        contacts.append(contact)
        input?.didUpdate(contacts: contacts)
    }
    
    func deleteItem(at index: Int) {
        contacts.remove(at: index)
        input?.didUpdate(contacts: contacts)
    }
    
}
