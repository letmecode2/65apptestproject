//
//  Contact.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

struct Contact {
    
    let id: String
    let name: String
    let surname: String?
    let dateOfBirth: String?
    let company: String?
    let email: String?

}
