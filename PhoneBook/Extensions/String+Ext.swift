//
//  String+Ext.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

extension String {
    
    var isEmail: Bool {
        
        do {
            
            let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            return regex.firstMatch(in: self,
                                    options: NSRegularExpression.MatchingOptions(rawValue: 0),
                                    range: NSRange(location: 0, length: count)) != nil
        }
        catch {
            
            return false
        }
    }
    
    var isPhoneNumber: Bool {
        
        do {
            
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSRange(location: 0, length: count))
            if let res = matches.first {
                
                return res.resultType == .phoneNumber
                    && res.range.location == 0
                    && res.range.length == count
            }
            else {
                
                return false
            }
        }
        catch {
            
            return false
        }
    }
}
