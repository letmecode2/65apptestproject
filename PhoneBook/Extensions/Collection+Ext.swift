//
//  Collection+Ext.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import Foundation

extension Collection where Index == Int {
    
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
}
