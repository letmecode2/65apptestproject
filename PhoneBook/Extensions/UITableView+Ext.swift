//
//  UITableView+Ext.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import UIKit

extension UITableView {
    
    static var reuseId: String {
        return String(describing: self)
    }
    
    func registerCell<T>(_ cellClass: T) {
        register(UINib(nibName: String(describing: cellClass), bundle: nil),
                 forCellReuseIdentifier: String(describing: cellClass))
    }
    
    func registerCells<T>(_ cellClass: [T]) {
        cellClass.forEach {
            registerCell($0)
        }
    }
    
    func dequeueReusableCell<CellClass: UITableViewCell>(of type: CellClass.Type,
                                                         for indexPath: IndexPath) -> CellClass {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: type),
                                             for: indexPath) as? CellClass else {
            fatalError("could not cast UITableViewCell at indexPath (section: \(indexPath.section), row: \(indexPath.row)) to expected type \(String(describing: CellClass.self))")
        }
        return cell
    }
    
    
}

