//
//  ContactsPresenterState.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

class ContactsPresenterState {
    
    var contacts = [Contact]()
    var searchContacts = [Contact]()
    var isSearching = false
}
