//
//  ContactsPresenter.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

class ContactsPresenter {
    
    // MARK: - Public properties
    weak var view: ContactsViewInput?
    
    private var state = ContactsPresenterState()
    private lazy var contactsService: ContactsManager = {
        let service = ContactsManager.shared
        service.input = self
        return service
    }()
    
}

// MARK: - ConnectionsViewOutput
extension ContactsPresenter: ContactsViewOutput {
    
    func viewDidLoad() {
        setupInitialState()
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        state.isSearching
        ? state.searchContacts.count
        : state.contacts.count
    }
    
    func cellModel(for index: Int) -> Contact? {
        state.isSearching
        ? state.searchContacts[safe: index]
        : state.contacts[safe: index]
        
    }
    
    func addTapped() {
        view?.showAddNewContactScreen()
    }
    
    func didSelectRowAt(index: Int) {
       guard let contact = state.isSearching
                ? state.searchContacts[safe: index]
                : state.contacts[safe: index] else { return }
        view?.showContactScreen(contact: contact)
    }
    
    func searchTextdidChange(_ text: String) {
        guard !text.isEmpty else { return }
        state.isSearching = true
        state.searchContacts = state.contacts.filter({ contact in
            return contact.name.lowercased().contains(text.lowercased())
            || ((contact.surname?.lowercased().contains(text.lowercased())) == true)
        })
        view?.reloadData()
    }
    
    func setIsSearching(_ isSearching: Bool) {
        state.isSearching = isSearching
        view?.reloadData()
    }
    
    func deleteItem(at index: Int) {
        contactsService.deleteItem(at: index)
    }
    
}

extension ContactsPresenter: ContactsManagerInput {
    
    func didUpdate(contacts: [Contact]) {
        state.contacts = contacts
        view?.reloadData()
    }
}

private extension ContactsPresenter {
    
    func setupInitialState() {
        state.contacts = contactsService.getContacts()
    }
}
