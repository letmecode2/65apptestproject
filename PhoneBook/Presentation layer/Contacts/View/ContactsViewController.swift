//
//  ContactsViewController.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import UIKit

class ContactsViewController: UIViewController {
    
    // MARK: - Private properties
    private let tableView = UITableView()
    
    private lazy var output: ContactsViewOutput = {
        
        let presenter = ContactsPresenter()
        presenter.view = self
        return presenter
    }()
    
    lazy var searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        setupView()
        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

}

extension ContactsViewController: UISearchControllerDelegate, UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchText = searchController.searchBar.text ?? ""
        output.searchTextdidChange(searchText)
    }
    
    
    func didDismissSearchController(_ searchController: UISearchController) {

        output.setIsSearching(false)
    }
    
}

// MARK: - ContactsViewInput
extension ContactsViewController: ContactsViewInput {
    
    func showAddNewContactScreen() {
        let addContactVC = AddContactViewController()
        addContactVC.type = .edit
        navigationController?.pushViewController(addContactVC, animated: true)
    }
    
    func showContactScreen(contact: Contact) {
        let addContactVC = AddContactViewController()
        addContactVC.type = .preview
        addContactVC.contact = contact
        navigationController?.pushViewController(addContactVC, animated: true)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
}

// MARK: - UITableViewDataSource
extension ContactsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfRowsInSection(section: section)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(of: ContactCell.self, for: indexPath)
        guard let model = output.cellModel(for: indexPath.row) else { return UITableViewCell() }
        cell.configure(with: model)
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
}

// MARK: - UITableViewDelegate
extension ContactsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        output.didSelectRowAt(index: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive,
                                        title: "Delete") { [unowned self] ( action, sourceView, completionHandler) in
            self.output.deleteItem(at: indexPath.row)
            completionHandler(true)
        }

        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        return swipeActionConfig
    }
    
}


// MARK: - Private methods
private extension ContactsViewController {
    
    func setupTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        tableView.backgroundColor = AppDesign.Color.background.ui
        tableView.tableFooterView = UIView()
        tableView.registerCell(ContactCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupView() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = AppDesign.Color.background.ui.withAlphaComponent(0.7)
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.compactAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
            navigationController?.navigationBar.isTranslucent = false
        }
        
        let addItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItem = addItem
        
        navigationItem.searchController = searchController
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
    }
    
    @objc func addTapped() {
        output.addTapped()
    }
                                    
}

