//
//  ContactsViewInput.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

protocol ContactsViewInput: AnyObject {
    
    func showAddNewContactScreen()
    func showContactScreen(contact: Contact)
    func reloadData()
    
}
