//
//  ContactsViewOutput.swift
//  PhoneBook
//
//  Created by сергей on 05.10.2021.
//

import Foundation

protocol ContactsViewOutput: AnyObject {
    
    func viewDidLoad()
    func numberOfRowsInSection(section: Int) -> Int
    func cellModel(for: Int) -> Contact?
    func addTapped()
    func didSelectRowAt(index: Int)
    func searchTextdidChange(_ text: String)
    func setIsSearching(_ isSearching: Bool)
    func deleteItem(at index: Int)
}
