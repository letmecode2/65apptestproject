//
//  ContactCell.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import UIKit

class ContactCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var surnameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with model: Contact) {
        nameLabel.text = model.name
        surnameLabel.text = model.surname
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
