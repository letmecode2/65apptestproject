//
//  AddContactViewOutput.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import Foundation

protocol AddContactViewOutput {
    
    func viewDidLoad(with contact: Contact?)
    func numberOfRowsInSection(section: Int) -> Int
    func cellModel(for: Int) -> ContactCellModel?
    func nameDidchange(_ name: String?)
    func surnameDidchange(_ surname: String?)
    func dateDidChange(_ date: String?)
    func companyDidChange(_ company: String?)
    func emailDidChange(_ email: String?)
    func saveTapped()
    
}
