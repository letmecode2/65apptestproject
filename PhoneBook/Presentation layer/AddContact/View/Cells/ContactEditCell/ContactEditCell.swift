//
//  ContactEditCell.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import UIKit

protocol ContactEditCellDelegate: AnyObject {
    
    func nameDidchange(_ name: String?)
    func surnameDidchange(_ surname: String?)
    func dateDidChange(_ date: String?)
    func companyDidChange(_ company: String?)
    func emailDidChange(_ email: String?)
}

class ContactEditCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var valueTF: UITextField!
    
    private var type: CellModelType?
    
    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        valueTF.inputView = picker
        return picker
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    
    weak var delegate: ContactEditCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        valueTF.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        valueTF.returnKeyType = .next
    }
    
    func configure(with model: ContactCellModel) {
        titleLabel.text = model.title
        valueTF.text = model.value
        type = model.type
        if type == .date {
            datePicker.addTarget(self, action: #selector(dateDidChange(_:)), for: .valueChanged)
        }
    }
    
    @objc func dateDidChange(_ datePicker: UIDatePicker) {
        let date = dateFormatter.string(from: datePicker.date)
        valueTF.text = date
        delegate?.dateDidChange(date)
    }
    
    @objc func textFieldDidChange(_ sender: UITextField) {
        
        switch type {
        case .name:
            delegate?.nameDidchange(sender.text)
        case .surname:
            delegate?.surnameDidchange(sender.text)
        case .company:
            delegate?.companyDidChange(sender.text)
        case .email:
            if !(sender.text?.isEmail ?? true) {
                sender.textColor = .red
            } else {
                sender.textColor = .black
            }
            delegate?.emailDidChange(sender.text)
        default:
            break
        }
    }
}
