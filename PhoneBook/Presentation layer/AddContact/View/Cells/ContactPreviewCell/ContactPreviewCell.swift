//
//  ContactPreviewCell.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import UIKit

class ContactPreviewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with model: ContactCellModel) {
        titleLabel.text = model.title
        valueLabel.text = model.value
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
