//
//  AddContactViewInput.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import Foundation

protocol AddContactViewInput: AnyObject {
    
    func reloadData()
    func popViewController()
    func saveIsActive(isActive: Bool)
    
}
