//
//  AddContactViewController.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import UIKit

enum ContactScrrenType {
    case preview
    case edit
}

class AddContactViewController: UIViewController {
    
    // MARK: - Private properties
    private let tableView = UITableView()
    private var addItem = UIBarButtonItem()
    
    private lazy var output: AddContactViewOutput = {
        let presenter = AddContactPresenter()
        presenter.view = self
        return presenter
    }()
    
    var type = ContactScrrenType.edit {
        didSet {
            configureRightBarItem()
        }
    }
    var contact: Contact?

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad(with: contact)
        setupView()
        setupTableView()
    }
}


// MARK: - ContactsViewInput
extension AddContactViewController: AddContactViewInput {
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func popViewController() {
        navigationController?.popViewController(animated: true)
    }
    
    func saveIsActive(isActive: Bool) {
        addItem.isEnabled = isActive
    }
}

// MARK: - UITableViewDataSource
extension AddContactViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.numberOfRowsInSection(section: section)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type {
        case .edit:
            let cell = tableView.dequeueReusableCell(of: ContactEditCell.self, for: indexPath)
            guard let model = output.cellModel(for: indexPath.row) else { return UITableViewCell() }
            cell.configure(with: model)
            cell.delegate = self
            return cell
        case .preview:
            let cell = tableView.dequeueReusableCell(of: ContactPreviewCell.self, for: indexPath)
            guard let model = output.cellModel(for: indexPath.row) else { return UITableViewCell() }
            cell.configure(with: model)
            return cell
        }
        
    }
}

// MARK: - ContactEditCellDelegate
extension AddContactViewController: ContactEditCellDelegate {
    
    func nameDidchange(_ name: String?) {
        output.nameDidchange(name)
    }
    
    func surnameDidchange(_ surname: String?) {
        output.surnameDidchange(surname)
    }
    
    func dateDidChange(_ date: String?) {
        output.dateDidChange(date)
    }
    
    func companyDidChange(_ company: String?) {
        output.companyDidChange(company)
    }
    
    func emailDidChange(_ email: String?) {
        output.emailDidChange(email)
    }
   
}


// MARK: - Private methods
private extension AddContactViewController {
    
    func setupTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        tableView.backgroundColor = AppDesign.Color.background.ui
        tableView.tableFooterView = UIView()
        tableView.registerCells([ContactPreviewCell.self,
                                 ContactEditCell.self])
        tableView.dataSource = self
        tableView.allowsSelection = false
    }
    
    func setupView() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = AppDesign.Color.background.ui.withAlphaComponent(0.7)
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.compactAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearance
            navigationController?.navigationBar.isTranslucent = false
        }
    }
    
    func configureRightBarItem() {
        if type == .edit {
            addItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
        } 
        navigationItem.rightBarButtonItem = addItem
    }
    
    @objc
    func saveTapped() {
        output.saveTapped()
    }
                                    
}


