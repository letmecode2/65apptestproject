//
//  AddContactPresenter.swift
//  PhoneBook
//
//  Created by сергей on 06.10.2021.
//

import Foundation

class AddContactPresenter {
    
    weak var view: AddContactViewInput?
    
    private var state = AddContactsPresenterState()
    private lazy var contactsService: ContactsManager = {
        let service = ContactsManager.shared
        return service
    }()
    
}

// MARK: - ConnectionsViewOutput
extension AddContactPresenter: AddContactViewOutput {
    
    func viewDidLoad(with contact: Contact?) {
        setupInitialState(with: contact)
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        state.contactItems.count
    }
    
    func cellModel(for index: Int) -> ContactCellModel? {
        return state.contactItems[safe: index]
    }
    
    func nameDidchange(_ name: String?) {
        changeContact(type: .name, with: name)
    }
    
    func surnameDidchange(_ surname: String?) {
        changeContact(type: .surname, with: surname)
    }
    
    func dateDidChange(_ date: String?) {
        changeContact(type: .date, with: date)
    }
    
    func companyDidChange(_ company: String?) {
        changeContact(type: .company, with: company)
    }
    
    func emailDidChange(_ email: String?) {
        changeContact(type: .email, with: email)
    }
    
    
    func saveTapped() {
        guard let contact = makeContactModel() else { return }
        contactsService.addContact(contact: contact)
        view?.popViewController()
    }
 
}

private extension AddContactPresenter {
    
    func setupInitialState(with contact: Contact?) {
        let nameItem = ContactCellModel(title: "Name", value: contact?.name, type: .name)
        let surnameItem = ContactCellModel(title: "Surname", value: contact?.surname, type: .surname)
        let date = ContactCellModel(title: "Date of birth", value: contact?.dateOfBirth, type: .date)
        let company = ContactCellModel(title: "Company", value: contact?.company, type: .company)
        let emailItem = ContactCellModel(title: "email", value: contact?.email, type: .email)
        state.contactItems = [nameItem, surnameItem, date, company, emailItem]
    }
    
    func changeContact(type: CellModelType, with value: String?) {
        state.contactItems.first(where: { $0.type == type })?.value = value
        setSaveButton()
    }
    
    func setSaveButton() {
        var isActive = false
        if let name = state.contactItems.first(where: { $0.type == .name })?.value,
           !name.isEmpty {
            isActive = true
            if let email = state.contactItems.first(where: { $0.type == .email })?.value,
               !email.isEmpty {
                isActive = email.isEmail
            }
        }
        view?.saveIsActive(isActive: isActive)
    }
    
    func makeContactModel() -> Contact? {
        guard let name = state.contactItems.first(where: { $0.type == .name })?.value else { return nil }
        let surname = state.contactItems.first(where: { $0.type == .surname })?.value
        let date = state.contactItems.first(where: { $0.type == .date })?.value
        let company = state.contactItems.first(where: { $0.type == .company })?.value
        let email = state.contactItems.first(where: { $0.type == .email })?.value
        let contact = Contact(id: UUID().uuidString,
                              name: name,
                              surname: surname,
                              dateOfBirth: date,
                              company: company,
                              email: email)
        return contact
    }
}


